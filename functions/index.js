const functions = require('firebase-functions');

exports.addednode = functions.database.ref('com_list/{pushId}/data')
    .onCreate((snapshot, context) =>{
        const original = snapshot.val() + 'QQ';
        return snapshot.ref.parent.child('addedQQdata').set(original);
    });


// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
